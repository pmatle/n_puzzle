/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spiral.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 15:03:26 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 13:50:19 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			first_row(t_data *data, int **array, t_state *state, int *x)
{
	int		count;
	t_data	temp;

	temp = *data;
	count = temp.start_col;
	while (count < temp.end_col)
	{
		(*array)[*x] = state->puzzle[temp.start_row][count];
		(*x)++;
		count++;
	}
	(*data).start_row++;
	return (1);
}

int			last_column(t_data *data, int **array, t_state *state, int *x)
{
	int		count;

	count = (*data).start_row;
	while (count < (*data).end_row)
	{
		(*array)[*x] = state->puzzle[count][(*data).end_col - 1];
		(*x)++;
		count++;
	}
	(*data).end_col--;
	return (1);
}

int			last_row(t_data *data, int **array, t_state *state, int *x)
{
	int		count;

	if ((*data).start_row < (*data).end_row)
	{
		count = (*data).end_col - 1;
		while (count >= (*data).start_col)
		{
			(*array)[*x] = state->puzzle[(*data).end_row - 1][count];
			(*x)++;
			count--;
		}
		(*data).end_row--;
	}
	return (1);
}

int			first_column(t_data *data, int **array, t_state *state, int *x)
{
	int		count;

	if ((*data).start_col < (*data).end_col)
	{
		count = (*data).end_row - 1;
		while (count >= (*data).start_row)
		{
			(*array)[*x] = state->puzzle[count][(*data).start_col];
			(*x)++;
			count--;
		}
		(*data).start_col++;
	}
	return (1);
}
