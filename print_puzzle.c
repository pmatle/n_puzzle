/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_puzzle.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 11:06:20 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/22 11:06:57 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		print_puzzle(t_state *state, int n)
{
	int		x;
	int		y;

	x = 0;
	while (x < n)
	{
		y = 0;
		while (y < n)
		{
			ft_putnbr(state->puzzle[x][y]);
			ft_putchar(' ');
			y++;
		}
		ft_putchar('\n');
		x++;
	}
	return (1);
}

int		search_and_replace(int n, int max)
{
	int		x;
	int		y;

	x = 0;
	while (x < n)
	{
		y = 0;
		while (y < n)
		{
			if (g_puzzle[x][y] == max)
			{
				g_puzzle[x][y] = 0;
				return (1);
			}
			y++;
		}
		x++;
	}
	return (0);
}
