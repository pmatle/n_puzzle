/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/24 16:52:31 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/27 12:07:15 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

void	malloc_properly(int ***puzzle, int n, char **list, char *file)
{
	if (!((*puzzle) = ft_alloc2d(n)))
	{
		free_array(list);
		free(file);
		ft_putendl(CRED"Error: Malloc Error");
		exit(0);
	}
}

int		**get_data(char *file, int *n)
{
	char	**list;
	char	**arr;
	int		**puzzle;
	int		x;
	int		y;

	x = 1;
	list = ft_strsplit(file, '\n');
	(*n) = ft_atoi(list[0]);
	malloc_properly(&puzzle, *n, list, file);
	while (x < (*n) + 1)
	{
		arr = ft_strsplit(list[x], ' ');
		y = 0;
		while (y < (*n))
		{
			puzzle[x - 1][y] = ft_atoi(arr[y]);
			y++;
		}
		free_array(arr);
		x++;
	}
	return (puzzle);
}

int		**get_puzzle(char *str, int *num)
{
	int		**puzzle;
	char	*file;
	char	*new;
	int		fd;
	int		x;

	x = 0;
	if ((fd = open(str, O_RDONLY)) == -1)
	{
		ft_putendl(CRED"Error: Function open failed"RSET);
		exit(0);
	}
	if (!(file = ft_strnew(1000)))
	{
		ft_putendl(CRED"Error: Malloc error"RSET);
		exit(0);
	}
	read(fd, file, 1000);
	new = organize(file);
	dimension_error(new);
	nonsense_check(new);
	puzzle = get_data(new, &(*num));
	return (puzzle);
}

char	*process_args(char **list, int size)
{
	int		x;
	char	*file;

	x = 3;
	if (ft_strequ(list[1], "-f"))
		file = list[2];
	while (x < size)
	{
		if (ft_strequ(list[x], "-h"))
		{
			if ((x + 1) < size)
				g_flags.heuristic = ft_atoi(list[x + 1]);
			x++;
		}
		else if (ft_strequ(list[x], "-v"))
			g_flags.visual = 1;
		else if (ft_strequ(list[x], "-p"))
		{
			g_flags.play = 1;
			break ;
		}
		x++;
	}
	g_flags.file = file;
	return (file);
}

int		main(int argc, char **argv)
{
	t_state		*state;
	int			**puzzle;
	int			num;
	char		*file;

	num = 0;
	if (argc > 1)
	{
		file = process_args(argv, argc);
		if (!file_error(file))
			return (0);
		if ((puzzle = get_puzzle(file, &num)) == NULL)
		{
			ft_putendl(CRED"Error: Something went wrong, try again"RSET);
			return (0);
		}
		check_errors(puzzle, num);
		state = set_state(puzzle, num, 0, 0);
		(g_flags.play) ? play(state) : 0;
		start_solving(state, puzzle);
	}
	else
		show_help();
	return (0);
}
