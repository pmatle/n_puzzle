/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visuals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 14:56:46 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 15:07:06 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

void		visual_mode(int **puzzle, t_direction *dir, char *ans)
{
	start_visualizer(puzzle, dir, 1);
	free(ans);
	free_2d(puzzle, dir->dimension);
	free_dir(dir);
	exit(0);
}
