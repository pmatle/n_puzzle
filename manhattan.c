/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manhattan.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 17:11:52 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 14:09:14 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			get_index(int dimension, int num, int *x, int *y)
{
	int		a;
	int		b;

	if (num != 0)
	{
		a = 0;
		while (a < dimension)
		{
			b = 0;
			while (b < dimension)
			{
				if (g_puzzle[a][b] == num)
				{
					(*x) = a;
					(*y) = b;
					return (1);
				}
				b++;
			}
			a++;
		}
	}
	(*x) = -1;
	(*y) = -1;
	return (0);
}

int			get_distance(int x1, int x2, int y1, int y2)
{
	int		distance;

	distance = 0;
	distance = abs(x1 - x2) + abs(y1 - y2);
	return (distance);
}

int			manhattan_distance(t_state *current)
{
	t_coord	coord;
	int		distance;
	int		x;
	int		y;

	x = 0;
	distance = 0;
	while (x < current->dimension)
	{
		y = 0;
		while (y < current->dimension)
		{
			if (current->puzzle[x][y] != 0)
			{
				get_index(current->dimension, current->puzzle[x][y],
					&coord.row, &coord.col);
				if (coord.row == -1 || coord.col == -1)
					return (-1);
				distance += get_distance(coord.row, x, coord.col, y);
			}
			y++;
		}
		x++;
	}
	return (distance);
}
