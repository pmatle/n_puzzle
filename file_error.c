/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_error.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 09:40:49 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/21 12:13:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		file_error(char *str)
{
	if (access(str, F_OK))
	{
		ft_putendl(CRED"Error: file specified is not found"RSET);
		return (0);
	}
	if (access(str, R_OK))
	{
		ft_putendl(CRED"Error: The are no read permissions on file"RSET);
		return (0);
	}
	if (is_directory(str))
	{
		ft_putendl(CRED"Error: path specified is a directory"RSET);
		return (0);
	}
	return (1);
}
