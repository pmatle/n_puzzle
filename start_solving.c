/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_solving.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 15:19:00 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 15:08:03 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

void	check_solvability(t_state *state)
{
	if (!(check_puzzle(state)))
	{
		ft_putendl(CRED"Error: The puzzle is unsolvable"RSET);
		free(state);
		exit(0);
	}
}

int		count_list(t_state *ans)
{
	int		x;

	x = 0;
	while (ans != NULL)
	{
		ans = ans->parent;
		x++;
	}
	return (x - 1);
}

int		free_dir(t_direction *dir)
{
	t_direction		*temp;

	if (dir != NULL)
	{
		while (dir != NULL)
		{
			temp = dir;
			if (temp->dir)
				free(temp->dir);
			dir = dir->next;
			if (temp)
				free(temp);
		}
	}
	return (1);
}

void	show_answer(t_state *ans, int **puzzle)
{
	char			*answer;
	t_direction		*dir;

	if (!(answer = ft_strnew(15)))
	{
		ft_putendl(CGRN"Huh, wasn't expecting that, please try again"RSET);
		return ;
	}
	dir = add_data(ans);
	print_solution(ans, dir);
	free_state(ans);
	(g_flags.visual) ? visual_mode(puzzle, dir, answer) : 0;
	ft_putstr(CGRN"\tDo you want to see visuals : ");
	scanf("%s", answer);
	ft_putstr(RSET);
	if (ft_strequ(answer, "yes") || ft_strequ(answer, "Yes"))
		start_visualizer(puzzle, dir, 1);
	free_2d(puzzle, dir->dimension);
	free_dir(dir);
	free(answer);
	exit(0);
}

int		start_solving(t_state *state, int **puzzle)
{
	t_state		*ans;

	get_goal_node(state->dimension);
	if (is_goal_puzzle(state))
	{
		ft_putendl(CGRN"The puzzle is already solved"RSET);
		free_state(state);
		free_2d(puzzle, state->dimension);
		exit(0);
	}
	check_solvability(state);
	if (!(ans = a_star(state)))
	{
		ft_putendl("A solution could not be found");
		return (0);
	}
	else
		show_answer(ans, puzzle);
	return (1);
}
