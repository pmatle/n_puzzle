/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_puzzle.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 11:42:12 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/25 10:30:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		swapping(t_state **state, char *dir, int x, int y)
{
	static int		flag = 0;

	if (ft_strequ(dir, "Left") && (x + 1) < (*state)->dimension)
	{
		flag = 26;
		ft_swap(&(*state)->puzzle[y][x], &(*state)->puzzle[y][x + 1]);
	}
	else if (ft_strequ(dir, "Right") && (x - 1) >= 0)
	{
		flag = 27;
		ft_swap(&(*state)->puzzle[y][x], &(*state)->puzzle[y][x - 1]);
	}
	else if (ft_strequ(dir, "Down") && (y - 1) >= 0)
	{
		flag = 28;
		ft_swap(&(*state)->puzzle[y][x], &(*state)->puzzle[y - 1][x]);
	}
	else if (ft_strequ(dir, "Up") && (y + 1) < (*state)->dimension)
	{
		flag = 29;
		ft_swap(&(*state)->puzzle[y][x], &(*state)->puzzle[y + 1][x]);
	}
	return (flag);
}

int		move(t_state **state, char *dir)
{
	int		index;
	int		flag;
	int		x;
	int		y;

	y = 0;
	while (y < (*state)->dimension)
	{
		x = 0;
		while (x < (*state)->dimension)
		{
			index = (*state)->puzzle[y][x];
			if (index == 0)
			{
				flag = swapping(state, dir, x, y);
				return (flag);
			}
			x++;
		}
		y++;
	}
	return (0);
}

int		arrange_numbers(t_state *state)
{
	int		index;
	int		x;
	int		y;
	int		count;

	y = 0;
	count = 0;
	while (y < state->dimension)
	{
		x = 0;
		while (x < state->dimension)
		{
			index = state->puzzle[y][x];
			g_head.tex = SDL_CreateTextureFromSurface(g_head.ren,
					g_head.img[index]);
			SDL_RenderCopy(g_head.ren, g_head.tex, NULL,
					&g_head.rect[count]);
			x++;
			count++;
		}
		y++;
	}
	return (1);
}

int		get_direction(t_state **state)
{
	static int		flag = 0;

	if (g_head.event.type == SDL_KEYDOWN)
	{
		if (g_head.event.key.keysym.sym == SDLK_LEFT)
		{
			flag = move(&(*state), "Left");
		}
		else if (g_head.event.key.keysym.sym == SDLK_RIGHT)
		{
			flag = move(&(*state), "Right");
		}
		else if (g_head.event.key.keysym.sym == SDLK_UP)
		{
			flag = move(&(*state), "Up");
		}
		else if (g_head.event.key.keysym.sym == SDLK_DOWN)
		{
			flag = move(&(*state), "Down");
		}
		g_head.dir = SDL_CreateTextureFromSurface(g_head.ren, g_head.img[flag]);
	}
	SDL_RenderCopy(g_head.ren, g_head.dir, NULL, &g_head.rect[25]);
	return (1);
}

void	make_puzzle(t_state *state)
{
	int		quit;

	quit = 0;
	if (!(init_game(state->dimension)))
		return ;
	while (!quit)
	{
		while (SDL_PollEvent(&g_head.event))
		{
			if (g_head.event.type == SDL_QUIT)
			{
				quit = 1;
				break ;
			}
			quit = (is_goal_puzzle(state)) ? solved(state) : 0;
			if (quit)
				break ;
			SDL_SetRenderDrawColor(g_head.ren, 255, 255, 255, 255);
			SDL_RenderClear(g_head.ren);
			get_direction(&state);
			arrange_numbers(state);
			SDL_RenderPresent(g_head.ren);
		}
	}
	end_game();
}
