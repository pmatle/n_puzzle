/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_goal_state.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/20 16:17:33 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/20 16:31:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

t_state		*set_goal_state(t_state *state, int **puzzle, int n)
{
	if (!(state = (t_state*)malloc(sizeof(*state))))
		return (NULL);
	state->dimension = n;
	add_puzzle(&state, puzzle);
	state->parent = NULL;
	return (state);
}
