/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_state.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 17:26:19 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 10:43:28 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		free_state(t_state *state)
{
	t_state		*temp;
	char		*dir;

	if (state != NULL)
	{
		temp = state;
		while (temp != NULL)
		{
			dir = temp->direction;
			temp = temp->parent;
			if (dir)
				free(dir);
		}
		free(state);
	}
	return (1);
}
