/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_depth.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 16:25:36 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 14:56:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		get_depth(t_priority_queue *head, t_state *goal)
{
	t_priority_queue	*temp;
	t_state				*state;

	temp = head;
	while (temp != NULL)
	{
		state = temp->state;
		if (is_goal(state, goal))
			return (state->depth);
		temp = temp->next;
	}
	return (-1);
}
