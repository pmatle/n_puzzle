/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 09:46:07 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/25 10:36:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

void	play(t_state *state)
{
	get_goal_node(state->dimension);
	make_puzzle(state);
	free_state(state);
	state = NULL;
	exit(0);
}
