/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_game.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 12:13:19 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/27 12:53:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		check_if_okay(int dimension)
{
	set_rectangles(dimension);
	g_head.ren = SDL_CreateRenderer(g_head.win, -1, SDL_RENDERER_ACCELERATED |
			SDL_RENDERER_PRESENTVSYNC);
	g_head.dir = SDL_CreateTextureFromSurface(g_head.ren, g_head.img[30]);
	return (1);
}

int		init_game(int dimension)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		set_images();
		if (dimension == 3)
		{
			g_head.win = SDL_CreateWindow("8-Puzzle Game", 300, 250, 440, 500,
					SDL_WINDOW_SHOWN);
		}
		if (dimension == 4)
		{
			g_head.win = SDL_CreateWindow("15-Puzzle Game", 300, 250, 540, 600,
					SDL_WINDOW_SHOWN);
		}
		if (dimension == 5)
		{
			g_head.win = SDL_CreateWindow("25-Puzzle Game", 200, 150, 640, 700,
					SDL_WINDOW_SHOWN);
		}
		set_rectangles(dimension);
		g_head.ren = SDL_CreateRenderer(g_head.win, -1, SDL_RENDERER_ACCELERATED
			| SDL_RENDERER_PRESENTVSYNC);
		g_head.dir = SDL_CreateTextureFromSurface(g_head.ren, g_head.img[30]);
		return (1);
	}
	return (0);
}

int		end_game(void)
{
	SDL_DestroyTexture(g_head.tex);
	SDL_DestroyTexture(g_head.dir);
	SDL_DestroyRenderer(g_head.ren);
	SDL_DestroyWindow(g_head.win);
	SDL_Quit();
	ft_putendl(CGRN"\e[1;1H\e[2JSDL exited successfully"RSET);
	return (0);
}
