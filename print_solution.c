/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_solution.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/20 18:43:37 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/22 17:46:43 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		print_solution(t_state *ans, t_direction *dir)
{
	t_direction		*temp;

	temp = dir;
	ft_putendl(CGRN"\n\tSolution Found : \n");
	ft_putstr("Complexity in Time :\t");
	ft_putnbr(g_complexity_time);
	ft_putchar('\n');
	ft_putstr("Complexity in Size :\t");
	ft_putnbr(g_complexity_size);
	ft_putchar('\n');
	ft_putstr("Number of Moves :   \t");
	ft_putnbr(count_list(ans));
	ft_putchar('\n');
	ft_putstr("Sequence of moves :\t");
	ft_putendl(temp->dir);
	temp = temp->next;
	while (temp != NULL)
	{
		ft_putstr("\t\t\t");
		ft_putendl(temp->dir);
		temp = temp->next;
	}
	ft_putstr("\n\n"RSET);
	return (1);
}
