/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   duplicate_puzzle.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 14:56:51 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/15 17:42:40 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		puzzle_duplicate(t_state **new, t_state *state, int n)
{
	int		x;
	int		y;
	int		temp;

	x = 0;
	while (x < n)
	{
		y = 0;
		while (y < n)
		{
			temp = state->puzzle[x][y];
			(*new)->puzzle[x][y] = temp;
			y++;
		}
		x++;
	}
	return (1);
}
