/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_help.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 11:02:48 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/25 09:51:07 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		show_help(void)
{
	ft_putstr(CGRN"Usage: ");
	ft_putendl("./n_puzzle [-f path] [-h heuristic] [-v] [-p]");
	ft_putstr("################################### Flags supported ");
	ft_putendl("############################");
	ft_putendl("\t\v-f : path: Specifies the path to the puzzle file");
	ft_putendl("\t-h : heuristic: Specifies the type of heuristic to use");
	ft_putendl("\t-v : show visuals of how the puzzle was solved");
	ft_putendl("\t-p : Interactive mode\v");
	ft_putstr("#####################################################");
	ft_putendl("###########################"RSET);
	return (1);
}
