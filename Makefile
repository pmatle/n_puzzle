# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/14 06:45:34 by pmatle            #+#    #+#              #
#    Updated: 2018/01/26 15:06:44 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = n_puzzle

SRC1 = main.c set_state.c is_directory.c ft_alloc2d.c file_error.c get_depth.c\
	start_solving.c get_goal_node.c move_tiles.c expand_node.c euclidean.c\
	duplicate_puzzle.c manhattan.c is_goal.c priority_queue.c stack.c a_star.c\
	change_node.c create_node.c check_puzzle.c free_2d.c spiral.c play.c\

SRC2 = misplaced_tiles.c get_heuristics.c init_game.c make_puzzle.c set_images.c\
	set_rectangles.c print_solution.c show_help.c start_visualizer.c animate.c\
	check_errors.c print_puzzle.c free_state.c directions.c duplicate_state.c\
	array_count.c free_array.c error.c error_misc.c visuals.c\

OBJ = main.o set_state.o is_directory.o ft_alloc2d.o file_error.o get_depth.o\
	start_solving.o get_goal_node.o move_tiles.o expand_node.o euclidean.o\
	duplicate_puzzle.o manhattan.o is_goal.o priority_queue.o stack.o a_star.o \
	change_node.o create_node.o check_puzzle.o free_2d.o spiral.o\
	misplaced_tiles.o get_heuristics.o init_game.o make_puzzle.o set_images.o\
	set_rectangles.o print_solution.o show_help.o start_visualizer.o animate.o\
	check_errors.o print_puzzle.o free_state.o directions.o duplicate_state.o\
	array_count.o free_array.o play.o error.o error_misc.o visuals.o\

CFLAGS = -Wall -Wextra -Werror

LIB = -L libft -lft

GCC = gcc

HEADER = n_puzzle.h

all: $(NAME)

$(NAME):
	@echo "Compiling..."
#	@make -C libft
	@$(GCC) $(CFLAGS) -c $(SRC1) $(SRC2) -I ~/.brew/include
	@gcc $(OBJ) $(LIB) -o $(NAME) -L ~/.brew/lib -lSDL2

clean:
	@echo "Cleaning object files..."
	@make clean -C libft
	@/bin/rm -f $(OBJ)

fclean: clean
	@echo "Deleting executable..."
#	@make fclean -C libft
	@/bin/rm -f $(NAME)

re: fclean all

norm:
	@printf "Norming all files...\n\nNormed files :\n"
	@norminette $(SRC1)
	@norminette $(SRC2) $(HEADER)
