/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 09:41:37 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/20 18:38:45 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			add(t_stack **head, t_state *state)
{
	t_stack		*new;

	if (!(new = (t_stack*)ft_memalloc(sizeof(*new))))
		return (0);
	new->state = state;
	puzzle_duplicate(&new->state, state, 3);
	new->next = (*head);
	(*head) = new;
	return (1);
}

int			search_stack(t_stack *stack, t_state *current)
{
	t_stack		*temp;
	t_state		*state;

	temp = stack;
	while (stack != NULL)
	{
		state = stack->state;
		if (is_goal(state, current))
			return (1);
		stack = stack->next;
	}
	return (0);
}

t_state		*remove_node(t_stack **head, t_state *new)
{
	t_stack		*current;
	t_stack		*prev;
	t_state		*state;

	current = (*head);
	if (is_goal(new, current->state))
	{
		(*head) = current->next;
		state = current->state;
		free(current);
		return (state);
	}
	while (current != NULL && current->next != NULL)
	{
		prev = current;
		current = current->next;
	}
	if (current == NULL)
		return (NULL);
	prev->next = current->next;
	state = current->state;
	free(current);
	return (state);
}

int			delete_stack(t_stack *stack)
{
	t_state		*state;

	while (stack != NULL)
	{
		state = remove_node(&stack, stack->state);
		free(state);
	}
	return (1);
}
