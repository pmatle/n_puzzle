/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   euclidean.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 09:24:57 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 09:39:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			get_euclidean_dist(int x1, int x2, int y1, int y2)
{
	int		distance;

	distance = 0;
	distance = ((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1));
	distance = (int)sqrt(distance);
	return (distance);
}

int			euclidean_distance(t_state *state)
{
	t_coord		coord;
	int			distance;
	int			y;
	int			x;

	y = 0;
	distance = 0;
	while (y < state->dimension)
	{
		x = 0;
		while (x < state->dimension)
		{
			if (state->puzzle[y][x] != 0)
			{
				get_index(state->dimension, state->puzzle[y][x], &coord.row,
					&coord.col);
				if (coord.row == -1 || coord.col == -1)
					return (-1);
				distance += get_euclidean_dist(coord.row, y, coord.col, x);
			}
			x++;
		}
		y++;
	}
	return (distance);
}
