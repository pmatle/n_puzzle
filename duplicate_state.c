/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   duplicate_state.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 15:56:49 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 16:08:57 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

t_state		*duplicate_state(t_state **state)
{
	t_state		*new;
	t_state		*temp;

	if (!(new = (t_state*)malloc(sizeof(*new))))
		return (NULL);
	temp = (*state);
	(*state) = new;
	return (temp);
}
