/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_heuristics.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 16:05:24 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 10:59:38 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		get_heuristic(t_state *state)
{
	int		heuristic;

	if (g_flags.heuristic == 1)
	{
		heuristic = manhattan_distance(state);
	}
	if (g_flags.heuristic == 2)
	{
		heuristic = euclidean_distance(state);
	}
	if (g_flags.heuristic == 3)
	{
		heuristic = misplaced_tile(state);
	}
	return (heuristic);
}
