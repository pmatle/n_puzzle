/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_state.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 14:47:19 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/19 14:51:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			add_puzzle(t_state **new, int **puzzle)
{
	t_state	*temp;
	int		y;
	int		x;
	int		num;

	y = 0;
	temp = (*new);
	while (y < temp->dimension)
	{
		x = 0;
		while (x < temp->dimension)
		{
			num = puzzle[y][x];
			temp->puzzle[y][x] = num;
			x++;
		}
		y++;
	}
	return (1);
}

t_state		*set_state(int **puzzle, int dimension, int depth, int fcost)
{
	t_state		*new;

	if (!(new = (t_state*)ft_memalloc(sizeof(*new))))
		return (0);
	new->dimension = dimension;
	add_puzzle(&new, puzzle);
	new->depth = depth;
	new->fcost = fcost;
	new->parent = NULL;
	return (new);
}
