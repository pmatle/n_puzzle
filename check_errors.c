/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 12:14:17 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 12:57:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			check(int **puzzle, int dimension, int num)
{
	int		x;
	int		y;

	y = 0;
	while (y < dimension)
	{
		x = 0;
		while (x < dimension)
		{
			if (puzzle[y][x] == num)
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}

void		unsupported_dimension(int dimension)
{
	if (dimension > 5 || dimension < 3)
	{
		ft_putendl(CRED"Error: Unsupported dimension"CGRN);
		exit(0);
	}
	if (g_flags.heuristic < 1 || g_flags.heuristic > 3)
	{
		ft_putendl(CRED"Error: Invalid Heuristic, enter number from 1 - 3");
		exit(0);
	}
}

void		check_errors(int **puzzle, int dimension)
{
	int		x;
	int		max;
	int		count;

	x = 0;
	count = 0;
	max = dimension * dimension;
	unsupported_dimension(dimension);
	while (x < max)
	{
		if (check(puzzle, dimension, count))
			count++;
		else
			break ;
		x++;
	}
	if (count != (dimension * dimension))
	{
		free_2d(puzzle, dimension);
		ft_putendl(CRED"Error: Incorrect file format"RSET);
		exit(0);
	}
}
