/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_goal_node.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 10:50:13 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/20 17:33:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		insert_numbers(int x, int y, t_var *var)
{
	y = x;
	while (y <= (*var).num - x - 1)
	{
		g_puzzle[x][y] = (*var).count++;
		y++;
	}
	y = x + 1;
	while (y <= (*var).num - x - 1)
	{
		g_puzzle[y][(*var).num - x - 1] = (*var).count++;
		y++;
	}
	y = (*var).num - x - 2;
	while (y >= x)
	{
		g_puzzle[(*var).num - x - 1][y] = (*var).count++;
		y--;
	}
	y = (*var).num - x - 2;
	while (y > x)
	{
		g_puzzle[y][x] = (*var).count++;
		y--;
	}
	return (1);
}

int		get_goal_node(int n)
{
	int		x;
	int		y;
	int		round;
	t_var	var;

	x = 0;
	y = 0;
	var.num = n;
	var.count = 1;
	round = ceil((double)n / 2);
	while (x < round)
	{
		insert_numbers(x, y, &var);
		x++;
	}
	search_and_replace(n, (n * n));
	return (1);
}
