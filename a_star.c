/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_star.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 09:15:34 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/28 14:44:48 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			children(t_state *state, t_priority_queue **open, t_stack **close,
		char *dir)
{
	int			depth;

	if (state != NULL)
	{
		state->direction = dir;
		if (!is_present((*open), state) &&
				!search_stack((*close), state))
		{
			state->direction = dir;
			add_node(&(*open), create_node(state));
		}
		else if (is_present((*open), state))
		{
			if ((depth = get_depth((*open), state)) == -1)
				return (0);
			if (state->depth < depth)
			{
				state->direction = dir;
				change_node(&(*open), state);
			}
		}
		return (1);
	}
	return (0);
}

int			initialization(t_state **state)
{
	t_state		*temp;

	temp = (*state);
	g_open = NULL;
	temp->depth = 0;
	g_complexity_size = 0;
	g_complexity_time = 0;
	temp->fcost = get_heuristic(temp);
	temp->parent = NULL;
	temp->direction = ft_strdup("start");
	add_node(&g_open, create_node(temp));
	return (1);
}

int			check_children(t_puzzle *all, t_stack **close)
{
	(!children((*all).up, &g_open, &(*close), ft_strdup("Up"))) ?
		free_state((*all).up) : 0;
	(!children((*all).down, &g_open, &(*close), ft_strdup("Down"))) ?
		free_state((*all).down) : 0;
	(!children((*all).left, &g_open, &(*close), ft_strdup("Left"))) ?
		free_state((*all).left) : 0;
	(!children((*all).right, &g_open, &(*close), ft_strdup("Right"))) ?
		free_state((*all).right) : 0;
	return (1);
}

t_state		*a_star(t_state *state)
{
	t_stack				*closed;
	t_state				*current;
	t_puzzle			all;

	closed = NULL;
	initialization(&state);
	while (g_open != NULL)
	{
		current = get_state(&g_open);
		g_complexity_time++;
		if (is_goal_puzzle(current))
		{
			delete_stack(closed);
			free_priority_queue(g_open);
			return (current);
		}
		else
		{
			expand_node(&all, current);
			check_children(&all, &closed);
		}
		add(&closed, current);
	}
	return (NULL);
}
