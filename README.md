# N-Puzzle

The goal of this project is to solve the N-puzzle game using the A* search algorithm or one of its variants. You start with a square board made up of N*N cells. One of these cells will be empty, the others will contain numbers, starting from 1, that will be unique in this instance of the puzzle. Your search algorithm will have to find a valid sequence of moves in order to reach the final state, a.k.a the "snail solution", which depends on the size of the puzzle.

## Compilation

Use the Makefile provided to compile the executable for the program.

```bash
make all
```

## Usage

```zsh
./n_puzzle [-f path] [-h heuristic] [-v] [-p]

# Flags supported:
#    -f : path: Specifies the path to the puzzle file
#    -h : heuristic: Specifies the type of heuristic to use
#    -v : show visuals of how the puzzle was solved
#    -p : Interactive mode
```

### Input File Example

```zsh
zaz@blackjack:~/npuzzle/$ cat -e npuzzle-3-1.txt
# this is a comment$
3$
3 2 6 #another comment$
1 4 0$
8 7 5$
zaz@blackjack:~/npuzzle/$ cat -e npuzzle-4-1.txt
# PONIES$
4$
0 10 5 7$
11 14 4 8$
1 2 6 13$
12 3 15 9$
zaz@blackjack:~/npuzzle/$ cat -e npuzzle-4-1.txt
# Puzzles can be aligned, or NOT. whatever. accept both.$
4$
0 10 5 7$
11 14 4 8$
1 2 6 13$
12 3 15 9$
zaz@blackjack:~/npuzzle/$
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

I you find any bugs while using the program please submit an issue so that the bug can be fixed.

## License
[MIT](https://choosealicense.com/licenses/mit/)