/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 14:36:27 by angonyam          #+#    #+#             */
/*   Updated: 2018/01/26 14:50:06 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int				wordcount(char *str)
{
	int			i;
	int			count;
	char		*c;

	c = ft_strtrim(str);
	if (ft_strlen(c) == 0)
		return (-1);
	count = 0;
	i = 0;
	while (c[i])
	{
		if (c[i] == ' ' || c[i] == '\t')
			count++;
		i++;
	}
	return (count);
}

char			*organize(char *str)
{
	t_num		t;
	char		**file;
	char		*p;

	t.catch = 0;
	file = ft_strsplit(str, '\n');
	t.i = skip_ahead(file);
	p = (char*)ft_memalloc(sizeof(char) * ft_strlen(str) + 2);
	t.k = 0;
	while (file[t.i])
	{
		t.j = -1;
		if (wordcount(file[t.i]) || ft_atoi(file[t.i]))
			t.catch = 1;
		if ((ft_strlen(file[t.i]) == 0 && t.catch == 1) ||
				(ft_strchr(file[t.i], '#') && t.catch == 1))
			error_haha();
		while (file[t.i][++t.j])
			p[t.k++] = file[t.i][t.j];
		p[t.k++] = '\n';
		t.i++;
	}
	free_array(file);
	p[t.k - 1] = '\0';
	return (p);
}

void			dimension_error(char *str)
{
	char		**file;
	char		**line;
	int			max;
	int			count;

	file = ft_strsplit(str, '\n');
	max = atoi(file[0]);
	count = array_cnt(str, '\n', 1);
	if (count != max)
		error_haha();
	line = ft_strsplit(file[1], ' ');
	count = array_cnt(file[1], ' ', 0);
	free_array(line);
	free_array(file);
	if (count != max)
		error_haha();
}

void			nonsense_check(char *str)
{
	int			i;
	char		**file;
	char		**ag;
	int			j;
	int			k;

	file = ft_strsplit(str, '\n');
	i = 0;
	while (file[i])
	{
		ag = ft_strsplit(file[i], ' ');
		j = 0;
		while (ag[j])
		{
			k = -1;
			while (ag[j][++k])
				if (!ft_isdigit(ag[j][k]))
					error_haha();
			j++;
		}
		free_array(ag);
		i++;
	}
	free_array(file);
}
