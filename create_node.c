/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_node.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 18:02:32 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 18:15:59 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

t_priority_queue	*create_node(t_state *state)
{
	t_priority_queue	*new;

	if (!(new = (t_priority_queue*)ft_memalloc(sizeof(*new))))
		return (NULL);
	new->priority = state->fcost;
	new->state = state;
	new->next = NULL;
	return (new);
}
