/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   n_puzzle.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/25 10:26:00 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/27 11:48:10 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef N_PUZZLE_H
# define N_PUZZLE_H

# include <fcntl.h>
# include <math.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <signal.h>
# include <SDL2/SDL.h>
# include "libft/includes/libft.h"

# define CRED "\x1b[31m"
# define CGRN "\x1b[32m"
# define CYLW "\x1b[31m"
# define RSET "\x1b[0m"

# define ROW 5
# define COL 5

typedef struct		s_state
{
	int				puzzle[ROW][COL];
	int				dimension;
	int				depth;
	int				fcost;
	char			*direction;
	struct s_state	*parent;
	struct s_state	*child;
}					t_state;

typedef struct		s_priority_queue
{
	t_state					*state;
	int						priority;
	struct s_priority_queue	*next;
}					t_priority_queue;

typedef struct		s_var
{
	int		num;
	int		count;
}					t_var;

typedef struct		s_puzzle
{
	t_state	*left;
	t_state	*right;
	t_state	*up;
	t_state	*down;
}					t_puzzle;

typedef struct		s_stack
{
	t_state			*state;
	struct s_stack	*next;
}					t_stack;

typedef struct		s_data
{
	int		start_col;
	int		end_col;
	int		start_row;
	int		end_row;
}					t_data;

typedef struct		s_coord
{
	int		row;
	int		col;
}					t_coord;

typedef struct		s_flags
{
	int		heuristic;
	int		visual;
	int		play;
	char	*file;
}					t_flags;

typedef struct		s_sdl2
{
	SDL_Event		event;
	SDL_Window		*win;
	SDL_Renderer	*ren;
	SDL_Surface		*img[33];
	SDL_Rect		rect[27];
	SDL_Texture		*tex;
	SDL_Texture		*dir;
}					t_sdl2;

typedef struct		s_direction
{
	char				*dir;
	int					dimension;
	struct s_direction	*next;
}					t_direction;

typedef struct		s_num
{
	int		i;
	int		j;
	int		k;
	int		catch;
}					t_num;

t_priority_queue	*g_open;
t_flags				g_flags;
t_sdl2				g_head;
int					g_puzzle[ROW][COL];
int					g_complexity_time;
int					g_complexity_size;

int					**ft_alloc2d(int num);
int					search_and_replace(int n, int max);
int					get_goal_node(int num);
int					is_goal(t_state *current, t_state *goal);
int					is_directory(char *str);
int					file_error(char *str);
t_state				*set_state(int **puzzle, int dimension, int depth,
		int fcost);
int					free_state(t_state *state);
int					start_solving(t_state *state, int **puzzle);
int					is_goal_puzzle(t_state *state);
int					get_empty_tile_pos(t_state *state, int *x, int *y);
int					print_puzzle(t_state *state, int num);
int					move_left(t_state **state, t_state *node, int x, int y);
int					move_right(t_state **state, t_state *node, int x, int y);
int					move_up(t_state **state, t_state *node, int x, int y);
int					move_down(t_state **state, t_state *node, int x, int y);
int					expand_node(t_puzzle *all, t_state *state);
int					manhattan_distance(t_state *current);
int					euclidean_distance(t_state *state);
t_state				*duplicate_state(t_state **state);
int					puzzle_duplicate(t_state **new, t_state *state, int n);
int					add(t_stack **head, t_state *state);
t_state				*remove_node(t_stack **head, t_state *state);
int					delete_stack(t_stack *stack);
t_priority_queue	*create_node(t_state *state);
int					free_priority_queue(t_priority_queue *queue);
t_state				*get_state(t_priority_queue **queue);
int					is_present(t_priority_queue *head, t_state *goal);
int					add_node(t_priority_queue **head, t_priority_queue *new);
int					search_stack(t_stack *stack, t_state *current);
int					get_depth(t_priority_queue *head, t_state *goal);
int					change_node(t_priority_queue **head, t_state *current);
t_state				*a_star(t_state *state);
int					first_row(t_data *data, int **array, t_state *state,
		int *x);
int					last_column(t_data *data, int **array, t_state *state,
		int *x);
int					last_row(t_data *data, int **array, t_state *state, int *x);
int					first_column(t_data *data, int **array, t_state *state,
		int *x);
int					check_puzzle(t_state *state);
t_state				*get_from_closed(t_stack **stack, t_state *goal);
int					free_2d(int **puzzle, int n);
int					add_puzzle(t_state **new, int **puzzle);
int					get_index(int dimension, int num, int *x, int *y);
int					misplaced_tile(t_state *state);
int					get_heuristic(t_state *state);
int					set_images(void);
int					free_array(char **list);
int					init_game(int dimension);
int					end_game(void);
int					array_count(char **list);
int					set_images(void);
void				play(t_state *state);
t_direction			*add_data(t_state *ans);
int					animate(t_state *state, t_direction *dir);
int					solved(t_state *state);
int					ft_swap(int *a, int *b);
int					print_solution(t_state *ans, t_direction *dir);
int					count_list(t_state *ans);
int					arrange_numbers(t_state *state);
void				make_puzzle(t_state *state);
int					set_rectangles(int dimension);
int					show_help(void);
int					**handle_puzzle(int fd, int **puzzle, int n);
int					move(t_state **state, char *dir);
void				check_errors(int **puzzle, int dimension);
int					start_visualizer(int **puzzle, t_direction *dir, int ans);
int					array_cnt(char *line, char delim, int num);
void				error_haha(void);
int					skip_ahead(char **file);
void				nonsense_check(char *str);
void				dimension_error(char *str);
char				*organize(char *str);
int					free_dir(t_direction *dir);
void				visual_mode(int **puzzle, t_direction *dir, char *ans);

#endif
