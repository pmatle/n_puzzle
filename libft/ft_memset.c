/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 18:09:47 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 16:38:27 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	character;
	char			*str;
	int				x;

	character = c;
	str = (char*)b;
	x = 0;
	while (len > 0)
	{
		str[x] = character;
		x++;
		len--;
	}
	return (str);
}
