/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_misc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 11:58:41 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 12:02:27 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int				skip_ahead(char **file)
{
	int		i;

	i = 0;
	while (ft_strchr(file[i], '#'))
		i++;
	return (i);
}

void			error_haha(void)
{
	printf("invalid file\n");
	exit(1);
}

int				array_cnt(char *line, char delim, int num)
{
	char		**arr;
	int			i;
	int			count;

	count = 0;
	arr = ft_strsplit(line, delim);
	i = num;
	while (arr[i])
	{
		count++;
		i++;
	}
	free_array(arr);
	return (count);
}
