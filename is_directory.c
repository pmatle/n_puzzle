/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_directory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 09:48:46 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 10:36:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		is_directory(char *path)
{
	struct stat	status;

	stat(path, &status);
	return (S_ISDIR(status.st_mode));
}
