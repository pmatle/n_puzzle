/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_rectangles.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:25:07 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 14:07:03 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

static void		set_rectangles_25(void)
{
	int		x;
	int		y;
	int		count;

	count = 0;
	x = 45;
	y = 15;
	while (count < 25)
	{
		if (count != 0 && (count % 5) == 0)
		{
			x = 45;
			y = y + 110;
		}
		g_head.rect[count].x = x;
		g_head.rect[count].y = y;
		g_head.rect[count].w = 100;
		g_head.rect[count].h = 100;
		x += 110;
		count++;
	}
	g_head.rect[count].x = 155;
	g_head.rect[count].y = 585;
	g_head.rect[count].w = 320;
	g_head.rect[count].h = 90;
}

static void		set_rectangles_15(void)
{
	int		x;
	int		y;
	int		count;

	count = 0;
	x = 45;
	y = 15;
	while (count < 16)
	{
		if (count != 0 && (count % 4) == 0)
		{
			x = 45;
			y = y + 110;
		}
		g_head.rect[count].x = x;
		g_head.rect[count].y = y;
		g_head.rect[count].w = 100;
		g_head.rect[count].h = 100;
		x += 110;
		count++;
	}
	g_head.rect[25].x = 105;
	g_head.rect[25].y = 475;
	g_head.rect[25].w = 270;
	g_head.rect[25].h = 80;
}

static void		set_rectangles_8(void)
{
	int		x;
	int		y;
	int		count;

	count = 0;
	x = 55;
	y = 22;
	while (count < 9)
	{
		if (count != 0 && (count % 3) == 0)
		{
			x = 55;
			y = y + 110;
		}
		g_head.rect[count].x = x;
		g_head.rect[count].y = y;
		g_head.rect[count].w = 100;
		g_head.rect[count].h = 100;
		x += 110;
		count++;
	}
	g_head.rect[25].x = 90;
	g_head.rect[25].y = 375;
	g_head.rect[25].w = 230;
	g_head.rect[25].h = 77;
}

int				set_rectangles(int dimension)
{
	if (dimension == 3)
		set_rectangles_8();
	else if (dimension == 4)
		set_rectangles_15();
	else if (dimension == 5)
		set_rectangles_25();
	return (1);
}
