/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_node.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 11:38:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/28 14:46:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		get_empty_tile_pos(t_state *state, int *x, int *y)
{
	int		a;
	int		b;

	a = 0;
	while (a < state->dimension)
	{
		b = 0;
		while (b < state->dimension)
		{
			if (state->puzzle[a][b] == 0)
			{
				(*x) = a;
				(*y) = b;
				return (1);
			}
			b++;
		}
		a++;
	}
	return (0);
}

t_state	*new_puzzle(t_state *new, t_state *state)
{
	new->dimension = state->dimension;
	new->depth = state->depth + 1;
	new->fcost = get_heuristic(new) + new->depth;
	new->parent = state;
	return (new);
}

int		reset_states(t_puzzle *all)
{
	all->down = NULL;
	all->left = NULL;
	all->right = NULL;
	all->up = NULL;
	return (1);
}

int		expand_node(t_puzzle *all, t_state *state)
{
	int			x;
	int			y;

	reset_states(all);
	if (get_empty_tile_pos(state, &x, &y))
	{
		if (move_right(&(*all).right, state, x, y))
		{
			(*all).right = new_puzzle((*all).right, state);
		}
		if (move_left(&(*all).left, state, x, y))
		{
			(*all).left = new_puzzle((*all).left, state);
		}
		if (move_down(&(*all).down, state, x, y))
		{
			(*all).down = new_puzzle((*all).down, state);
		}
		if (move_up(&(*all).up, state, x, y))
		{
			(*all).up = new_puzzle((*all).up, state);
		}
	}
	return (1);
}
