/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_tiles.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 12:29:32 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 12:10:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		move_left(t_state **state, t_state *node, int y, int x)
{
	int		tmp;
	t_state	*new;

	if (*state == NULL)
	{
		if (!(new = (t_state*)ft_memalloc(sizeof(*new))))
			return (0);
	}
	puzzle_duplicate(&new, node, node->dimension);
	if ((x + 1) < node->dimension)
	{
		tmp = new->puzzle[y][x + 1];
		new->puzzle[y][x + 1] = new->puzzle[y][x];
		new->puzzle[y][x] = tmp;
		(*state) = new;
		return (1);
	}
	return (0);
}

int		move_right(t_state **state, t_state *node, int y, int x)
{
	int		tmp;
	t_state	*new;

	if (*state == NULL)
	{
		if (!(new = (t_state*)ft_memalloc(sizeof(*new))))
			return (0);
	}
	puzzle_duplicate(&new, node, node->dimension);
	if ((x - 1) >= 0)
	{
		tmp = new->puzzle[y][x - 1];
		new->puzzle[y][x - 1] = new->puzzle[y][x];
		new->puzzle[y][x] = tmp;
		(*state) = new;
		return (1);
	}
	return (0);
}

int		move_down(t_state **state, t_state *node, int y, int x)
{
	int		tmp;
	t_state	*new;

	if (*state == NULL)
	{
		if (!(new = (t_state*)ft_memalloc(sizeof(*new))))
			return (0);
	}
	puzzle_duplicate(&new, node, node->dimension);
	if ((y - 1) >= 0)
	{
		tmp = new->puzzle[y - 1][x];
		new->puzzle[y - 1][x] = new->puzzle[y][x];
		new->puzzle[y][x] = tmp;
		(*state) = new;
		return (1);
	}
	return (0);
}

int		move_up(t_state **state, t_state *node, int y, int x)
{
	int		tmp;
	t_state	*new;

	if (*state == NULL)
	{
		if (!(new = (t_state*)ft_memalloc(sizeof(*new))))
			return (0);
	}
	puzzle_duplicate(&new, node, node->dimension);
	if ((y + 1) < node->dimension)
	{
		tmp = new->puzzle[y + 1][x];
		new->puzzle[y + 1][x] = new->puzzle[y][x];
		new->puzzle[y][x] = tmp;
		(*state) = new;
		return (1);
	}
	return (0);
}
