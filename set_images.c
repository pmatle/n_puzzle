/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_images.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:02:18 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 18:17:41 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			reset_images(void)
{
	int		x;

	x = 0;
	while (x < 31)
	{
		g_head.img[x] = NULL;
		x++;
	}
	return (1);
}

int			remaining_imgs(void)
{
	g_head.img[22] = SDL_LoadBMP("Numbers/number_22.bmp");
	g_head.img[23] = SDL_LoadBMP("Numbers/number_23.bmp");
	g_head.img[24] = SDL_LoadBMP("Numbers/number_24.bmp");
	g_head.img[25] = SDL_LoadBMP("Numbers/number_25.bmp");
	g_head.img[26] = SDL_LoadBMP("Numbers/Left.bmp");
	g_head.img[27] = SDL_LoadBMP("Numbers/Right.bmp");
	g_head.img[28] = SDL_LoadBMP("Numbers/Down.bmp");
	g_head.img[29] = SDL_LoadBMP("Numbers/Up.bmp");
	g_head.img[30] = SDL_LoadBMP("Numbers/Start.bmp");
	g_head.img[31] = SDL_LoadBMP("Numbers/Solved.bmp");
	return (1);
}

int			set_images(void)
{
	reset_images();
	g_head.img[1] = SDL_LoadBMP("Numbers/number_1.bmp");
	g_head.img[2] = SDL_LoadBMP("Numbers/number_2.bmp");
	g_head.img[3] = SDL_LoadBMP("Numbers/number_3.bmp");
	g_head.img[4] = SDL_LoadBMP("Numbers/number_4.bmp");
	g_head.img[5] = SDL_LoadBMP("Numbers/number_5.bmp");
	g_head.img[6] = SDL_LoadBMP("Numbers/number_6.bmp");
	g_head.img[7] = SDL_LoadBMP("Numbers/number_7.bmp");
	g_head.img[8] = SDL_LoadBMP("Numbers/number_8.bmp");
	g_head.img[9] = SDL_LoadBMP("Numbers/number_9.bmp");
	g_head.img[10] = SDL_LoadBMP("Numbers/number_10.bmp");
	g_head.img[11] = SDL_LoadBMP("Numbers/number_11.bmp");
	g_head.img[12] = SDL_LoadBMP("Numbers/number_12.bmp");
	g_head.img[13] = SDL_LoadBMP("Numbers/number_13.bmp");
	g_head.img[14] = SDL_LoadBMP("Numbers/number_14.bmp");
	g_head.img[15] = SDL_LoadBMP("Numbers/number_15.bmp");
	g_head.img[16] = SDL_LoadBMP("Numbers/number_16.bmp");
	g_head.img[17] = SDL_LoadBMP("Numbers/number_17.bmp");
	g_head.img[18] = SDL_LoadBMP("Numbers/number_18.bmp");
	g_head.img[19] = SDL_LoadBMP("Numbers/number_19.bmp");
	g_head.img[20] = SDL_LoadBMP("Numbers/number_20.bmp");
	g_head.img[21] = SDL_LoadBMP("Numbers/number_21.bmp");
	remaining_imgs();
	return (1);
}
