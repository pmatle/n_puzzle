/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misplaced_tiles.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 15:18:29 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/20 17:40:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		misplaced_tile(t_state *state)
{
	int		misplaced;
	int		y;
	int		x;

	y = 0;
	misplaced = 0;
	while (y < state->dimension)
	{
		x = 0;
		while (x < state->dimension)
		{
			if (g_puzzle[y][x] != state->puzzle[y][x])
				misplaced++;
			x++;
		}
		y++;
	}
	return (misplaced);
}
