/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 15:00:17 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/24 12:22:01 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int				insert(t_direction **head, char *dir, int dimension)
{
	t_direction		*new;

	if (!(new = (t_direction*)malloc(sizeof(*new))))
		return (0);
	new->dir = ft_strdup(dir);
	new->dimension = dimension;
	new->next = (*head);
	(*head) = new;
	return (0);
}

t_direction		*add_data(t_state *ans)
{
	t_state		*temp;
	t_direction	*dir;

	dir = NULL;
	temp = ans;
	while (temp->parent != NULL)
	{
		insert(&dir, temp->direction, temp->dimension);
		temp = temp->parent;
	}
	return (dir);
}
