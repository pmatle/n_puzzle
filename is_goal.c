/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_goal.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 11:23:31 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/20 17:48:08 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		is_goal(t_state *state, t_state *goal)
{
	int		x;
	int		y;

	x = 0;
	while (x < state->dimension)
	{
		y = 0;
		while (y < state->dimension)
		{
			if (goal->puzzle[x][y] != state->puzzle[x][y])
				return (0);
			y++;
		}
		x++;
	}
	return (1);
}

int		is_goal_puzzle(t_state *state)
{
	int		x;
	int		y;

	x = 0;
	while (x < state->dimension)
	{
		y = 0;
		while (y < state->dimension)
		{
			if (g_puzzle[x][y] != state->puzzle[x][y])
				return (0);
			y++;
		}
		x++;
	}
	return (1);
}
