/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_visualizer.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 16:04:38 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 15:47:26 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int			solved(t_state *state)
{
	SDL_SetRenderDrawColor(g_head.ren, 255, 255, 255, 255);
	SDL_RenderClear(g_head.ren);
	arrange_numbers(state);
	g_head.dir = SDL_CreateTextureFromSurface(g_head.ren, g_head.img[31]);
	SDL_RenderCopy(g_head.ren, g_head.dir, NULL, &g_head.rect[25]);
	SDL_RenderPresent(g_head.ren);
	SDL_Delay(2000);
	return (1);
}

int			start_visualizer(int **puzzle, t_direction *dir, int ans)
{
	t_state			*state;
	t_state			*temp;
	t_direction		*tmp_dir;

	if (!(state = set_state(puzzle, dir->dimension, 0, 0)))
	{
		ft_putendl(CGRN"Error: Something went wrong, try again"RSET);
		return (0);
	}
	temp = state;
	tmp_dir = dir;
	if (ans == 1)
	{
		ft_putendl("Starting animating");
		animate(temp, tmp_dir);
		ft_putendl("Finished animating");
	}
	else
		make_puzzle(temp);
	if (state)
		free_state(state);
	return (1);
}
