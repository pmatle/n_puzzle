/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_node.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 16:32:51 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/23 13:01:30 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		change_node(t_priority_queue **head, t_state *current)
{
	t_priority_queue	*temp;
	t_state				*state;

	temp = (*head);
	while (temp != NULL)
	{
		state = temp->state;
		if (is_goal(state, current))
		{
			state->depth = current->depth;
			state->direction = current->direction;
			state->parent = current->parent;
			state->fcost = current->fcost;
			return (1);
		}
		temp = temp->next;
	}
	return (0);
}
