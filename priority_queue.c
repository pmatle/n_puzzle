/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   priority_queue.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 09:28:19 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/28 14:55:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		search_and_add(t_priority_queue **head, t_priority_queue *new)
{
	t_priority_queue	*current;
	t_priority_queue	*prev;

	current = (*head);
	prev = current;
	current = current->next;
	while (current != NULL)
	{
		if (current->priority > new->priority)
		{
			prev->next = new;
			new->next = current;
			return (1);
		}
		prev = current;
		current = current->next;
	}
	prev->next = new;
	return (1);
}

int		add_node(t_priority_queue **head, t_priority_queue *new)
{
	g_complexity_size++;
	if (*head == NULL)
		(*head) = new;
	else
	{
		if ((*head)->priority > new->priority)
		{
			new->next = (*head);
			(*head) = new;
			return (1);
		}
		if (search_and_add(&(*head), new))
			return (1);
	}
	return (0);
}

t_state	*get_state(t_priority_queue **queue)
{
	t_state				*state;
	t_priority_queue	*temp;

	temp = (*queue);
	(*queue) = (*queue)->next;
	state = temp->state;
	return (state);
}

int		is_present(t_priority_queue *head, t_state *goal)
{
	t_priority_queue	*queue;
	t_state				*state;

	queue = head;
	while (queue != NULL)
	{
		state = queue->state;
		if (is_goal(state, goal))
			return (1);
		queue = queue->next;
	}
	return (0);
}

int		free_priority_queue(t_priority_queue *queue)
{
	t_state				*state;
	t_priority_queue	*temp;

	while (queue != NULL)
	{
		state = queue->state;
		free(state);
		temp = queue;
		queue = queue->next;
		free(temp);
	}
	return (1);
}
