/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alloc2d.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 14:41:07 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/26 12:32:22 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int		**ft_alloc2d(int num)
{
	int		**puzzle;
	int		x;

	x = 0;
	if (!(puzzle = (int**)ft_memalloc(sizeof(int*) * num)))
		return (NULL);
	while (x < num)
	{
		if (!(puzzle[x] = (int*)ft_memalloc(sizeof(int) * num)))
			return (NULL);
		x++;
	}
	return (puzzle);
}
