/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_puzzle.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 11:18:50 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/15 15:50:16 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

t_data		initialize_data(int row, int col)
{
	t_data		data;

	data.start_col = 0;
	data.start_row = 0;
	data.end_col = col;
	data.end_row = row;
	return (data);
}

int			*get_array(t_state *state)
{
	t_data		data;
	int			*array;
	int			x;

	x = 0;
	data = initialize_data(state->dimension, state->dimension);
	array = (int*)ft_memalloc(sizeof(*array) * (data.end_col * data.end_row));
	if (array == NULL)
		return (0);
	while (data.start_col < data.end_col && data.start_row < data.end_row)
	{
		first_row(&data, &array, state, &x);
		last_column(&data, &array, state, &x);
		last_row(&data, &array, state, &x);
		first_column(&data, &array, state, &x);
	}
	return (array);
}

int			locate_empty_pos(t_state *state)
{
	int		x;
	int		y;

	y = state->dimension - 1;
	while (y >= 0)
	{
		x = state->dimension - 1;
		while (x >= 0)
		{
			if (state->puzzle[y][x] == 0)
				return (state->dimension - y);
			x--;
		}
		y--;
	}
	return (-1);
}

int			count_inversions(t_state *state, int *array)
{
	int		inversions;
	int		n;
	int		x;
	int		y;

	x = 0;
	inversions = 0;
	n = state->dimension * state->dimension;
	while (x < (n - 1))
	{
		y = x + 1;
		while (y < n)
		{
			if (array[y] && array[x] && array[x] > array[y])
				inversions++;
			y++;
		}
		x++;
	}
	return (inversions);
}

int			check_puzzle(t_state *state)
{
	int			*array;
	int			inversion;
	int			empty_piece_pos;

	array = get_array(state);
	inversion = count_inversions(state, array);
	if (state->dimension & 1)
		return (!(inversion & 1));
	else
	{
		empty_piece_pos = locate_empty_pos(state);
		if (empty_piece_pos & 1)
			return (!(inversion & 1));
		else
			return (inversion & 1);
	}
}
