/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   animate.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 15:59:20 by pmatle            #+#    #+#             */
/*   Updated: 2018/01/27 11:52:53 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "n_puzzle.h"

int				render(t_direction **dir, t_state *state)
{
	SDL_RenderPresent(g_head.ren);
	(*dir) = (*dir)->next;
	if ((*dir) == NULL)
	{
		solved(state);
		SDL_Delay(2000);
		return (1);
	}
	return (0);
}

int				update(t_state **state, t_direction *dir)
{
	int			img;

	img = 0;
	SDL_SetRenderDrawColor(g_head.ren, 255, 255, 255, 255);
	SDL_RenderClear(g_head.ren);
	if (ft_strequ(dir->dir, "Left"))
		img = move(&(*state), "Left");
	else if (ft_strequ(dir->dir, "Right"))
		img = move(&(*state), "Right");
	else if (ft_strequ(dir->dir, "Up"))
		img = move(&(*state), "Up");
	else if (ft_strequ(dir->dir, "Down"))
		img = move(&(*state), "Down");
	arrange_numbers(*state);
	g_head.dir = SDL_CreateTextureFromSurface(g_head.ren, g_head.img[img]);
	SDL_RenderCopy(g_head.ren, g_head.dir, NULL, &g_head.rect[25]);
	return (1);
}

int				animate(t_state *state, t_direction *dir)
{
	int			fps;
	int			quit;
	int			frame_delay;
	uint32_t	frame_start;
	int			frame_time;

	fps = 60;
	quit = 0;
	frame_delay = 30000 / fps;
	if (!(init_game(state->dimension)))
		return (0);
	while (!quit)
	{
		frame_start = SDL_GetTicks();
		SDL_PollEvent(&g_head.event);
		update(&state, dir);
		quit = render(&dir, state);
		frame_time = SDL_GetTicks() - frame_start;
		if (frame_delay > frame_time)
		{
			SDL_Delay(frame_delay - frame_time);
		}
	}
	end_game();
	return (1);
}
